export const createRandomColor = (): string => {
    // 0부터 255 사이의 무작위 정수 생성
    const randomInt = () => Math.floor(Math.random() * 256);

    // 무작위 RGB 값 생성
    const red = randomInt();
    const green = randomInt();
    const blue = randomInt();

    // RGB 값을 16진수로 변환하여 색상 문자열 생성
    const color = `#${red.toString(16).padStart(2, '0')}${green.toString(16).padStart(2, '0')}${blue.toString(16).padStart(2, '0')}`;

    return color;
};