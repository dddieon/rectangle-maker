import { Component, onMount } from 'solid-js';
import styles from './App.module.css';
import Editor from './components/Editor';
import ContextMenu from './components/ContextMenu';

const App: Component = () => {
    onMount(() => {
        document.body.addEventListener('contextmenu', (event) => {
            event.preventDefault();
        });
    });

    return (
        <div class={styles.App}>
            <Editor />
            <Editor />
            <ContextMenu />
        </div>
    );
};

export default App;
