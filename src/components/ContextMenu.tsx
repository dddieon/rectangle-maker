import { Show } from 'solid-js';
import { contextSys } from '../system/Context';
import { boxSys } from '../system/Box';

const ContextMenu = () => {
    return (
        <Show when={contextSys.state.open && boxSys.state.shape}>
            <div
                style={{
                    position: 'absolute',
                    color: 'white',
                    left: `${contextSys.state.position?.x}px`,
                    top: `${contextSys.state.position?.y}px`,
                    'z-index': 111,
                }}
            >
                {typeof contextSys.state.selectedIndex === 'number' ? (
                    <div>
                        <button type={'button'} onClick={() => boxSys.deleteBox()}>
                            삭제
                        </button>
                        <button type={'button'} onClick={() => boxSys.handleChangeColor()}>
                            컬러변경
                        </button>
                    </div>
                ) : (
                    <div>
                        <button type={'button'} onClick={(e) => boxSys.addBox(e)}>
                            추가
                        </button>
                        {/* 이슈 : boxSys.addBox 을 그냥 넣으면 this.set 이 Undefined라서 class에서 에러났음 */}
                    </div>
                )}
            </div>
        </Show>
    );
};

export default ContextMenu;
