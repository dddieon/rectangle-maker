import { boxSys } from '../system/Box';
import { createEffect, For } from 'solid-js';

import { contextSys } from '../system/Context';
import Rectangle from './Rectangle';

const Editor = () => {
    createEffect(() => {
        document.addEventListener('contextmenu', (e) => contextSys.showContextMenu(e));
        document.addEventListener('click', contextSys.hideContextMenu);
        return () => {
            document.removeEventListener('contextmenu', (e) => contextSys.showContextMenu(e));
            document.removeEventListener('click', contextSys.hideContextMenu);
        };
    });

    return (
        <div style={{ position: 'relative' }}>
            <For each={boxSys.state.shape}>
                {(shapeKey, index) => {
                    return (
                        <Rectangle
                            shapeKey={shapeKey}
                            style={{
                                'background-color': shapeKey.color,
                                transform: `translate(${shapeKey.position.x}px, ${shapeKey.position.y}px)`,
                            }}
                            {...{ 'box-index': index() }}
                        />
                    );
                }}
            </For>
        </div>
    );
};

export default Editor;
