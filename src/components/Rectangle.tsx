import { style } from '@macaron-css/core';
import { TBox } from '../system/Box';
import { JSX } from 'solid-js/jsx-runtime';

const Rectangle = (props: { shapeKey: TBox['shape'][number]; style?: JSX.CSSProperties }) => {
    return (
        <div
            class={style({
                position: 'fixed',
                display: 'inline-block',
                height: '50px',
                width: '50px',
                // backgroundColor: shapeKey.color,  => error!
                // transform: `translate(${shapeKey.position.x}px, ${shapeKey.position.y}px)`,  => error!
            })}
            {...props}
        />
    );
};

export default Rectangle;
