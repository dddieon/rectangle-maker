import { createStore, produce, SetStoreFunction } from 'solid-js/store';
import { contextSys } from './Context';
import { createRandomColor } from '../utility/Color';

export type TBox = { shape: { color: string; position: { x: number; y: number } }[] };

const defaultBox: TBox = { shape: [] };

class BoxSys {
    state: TBox;
    set: SetStoreFunction<TBox>;

    constructor() {
        [this.state, this.set] = createStore<TBox>(defaultBox);
    }

    handleChangeColor() {
        this.set(
            'shape',
            boxSys.state.shape.map((box, index) => {
                if (index === contextSys.state.selectedIndex) {
                    return {
                        ...box,
                        color: createRandomColor(),
                    };
                }
                return box;
            }),
        );
    }

    deleteBox() {
        this.set(
            'shape',
            boxSys.state.shape.filter((_, index) => index !== contextSys.state.selectedIndex),
        );
    }

    addBox(e: MouseEvent) {
        const target = e.target as HTMLElement;
        const previousSibling = target?.parentElement?.parentElement
            ?.previousElementSibling as HTMLElement;

        if (!previousSibling) {
            console.error("There's no previous sibling element.");
            return;
        }

        // 클릭된 요소의 직전 형제 요소를 기준으로하여 상대적인 위치 계산
        const siblingBoundingRect = previousSibling.getBoundingClientRect();
        const relativeY =
            e.clientY < siblingBoundingRect.height
                ? e.clientY
                : e.clientY - siblingBoundingRect.height;

        this.set(
            produce(
                (state) =>
                    (state.shape = [
                        ...state.shape,
                        {
                            position: {
                                x: e.clientX,
                                y: relativeY,
                            },
                            color: createRandomColor(),
                        },
                    ]),
            ),
        );
    }
}

export const boxSys = new BoxSys();
