import { createStore, SetStoreFunction } from 'solid-js/store';

type TContext = {
    open: boolean;
    position: null | { x: number; y: number };
    selectedIndex: null | number;
};
const defaultBox = { open: false, position: null, selectedIndex: null };

class ContextSys {
    state: TContext;
    set: SetStoreFunction<TContext>;

    constructor() {
        [this.state, this.set] = createStore<TContext>(defaultBox);
    }

    private setOpen(boolean: boolean) {
        this.set('open', boolean);
    }

    private setByPosition(x: number, y: number) {
        this.set('position', { x, y });
    }

    private setSelectedIndex(number: number | null) {
        this.set('selectedIndex', number);
    }

    public showContextMenu(e: MouseEvent) {
        e.preventDefault();
        const target = e?.target as HTMLElement;
        const current = target.getAttribute('box-index');
        contextSys.setOpen(true);
        contextSys.setByPosition(e.clientX, e.clientY);
        contextSys.setSelectedIndex(current === null ? null : +current);
    }

    public hideContextMenu() {
        contextSys.setOpen(false);
        contextSys.setSelectedIndex(null);
    }
}

export const contextSys = new ContextSys();
